<?php
namespace Classes\Format;

require_once('classes/format/XmlFormat.php');
require_once('classes/format/JsonFormat.php');
require_once('classes/format/CsvFormat.php');
require_once('classes/format/DefaultFormat.php');

use Classes\Format\XmlFormat;
use Classes\Format\JsonFormat;
use Classes\Format\CsvFormat;
use Classes\Format\DefaultFormat;

class Format {
	
	public function getXml(object $data) {
		$xml_format = new XmlFormat;
		$xml_format->getFormatData($data);
	}

	public function getJson(object $data) {
		$json_format = new JsonFormat;
		$json_format->getFormatData($data);
	}

	public function getCsv(object $data) {
		$csv_format = new CsvFormat;
		$csv_format->getFormatData($data);
	}

	public function getDefault(object $data) {
		$default_format = new DefaultFormat;
		$default_format->getFormatData($data);
	}
}
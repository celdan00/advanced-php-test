<?php
namespace Classes\Format;

require_once('classes/interface/FormatInterface.php');

use Classes\interface\FormatInterface;

class JsonFormat implements FormatInterface {

	public function getFormatData(object $data) {
		header('Content-type: application/json');
        echo json_encode($data->all());
	}
}
<?php
namespace Classes\Players;

require_once('database/classes/Database.php');
use Classes\Database;

class Players {

	public function __construct() {
		$this->db = new Database;
	}

	protected function getSearchParameter(object $search) {
		$where = [];
		$search_array_key = array_keys($search->toArray());
		$search_array_value = array_values($search->toArray());

		switch ($search_array_key[0]) {
			case 'playerId':
				$where[] = "roster.id = '" . $search_array_value[0] . "'";
				break;
			case 'player': 
				$where[] = "roster.name = '" . $search_array_value[0] . "'";
				break;
			case 'team': 
				$where[] = "roster.team_code = '" . $search_array_value[0] . "'";
				break;
			case 'position': 
				$where[] = "roster.pos = '" . $search_array_value[0] . "'";
				break;
			case 'country':
				$where[] = "roster.nationality = '" . $search_array_value[0] . "'"; 
				break;
			default:
				throw new Exception("Sorry you've passed a wrong parameter");
				break;
		}

        return  $where = implode(' AND ', $where);
	}

}
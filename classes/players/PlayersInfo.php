<?php
namespace Classes\Players;

require_once('classes/players/Players.php');
require_once('classes/interface/PlayersInterface.php');

use Classes\interface\PlayersInterface;
use Classes\Players\Players;

class PlayersInfo extends Players implements PlayersInterface{

	public function getPlayerDetails(object $search) {
        $where = $this->getSearchParameter($search);
        $sql = "
            SELECT roster.*
            FROM roster
            WHERE $where";
        
        return collect($this->db->query($sql))
            ->map(function($item, $key) {
                unset($item['id']);
                return $item;
            });
    }

	
}
<?php
namespace Classes;

// retrieves & formats data from the database for export
class Exporter {
    
    private $player_stats;
    private $players_info;
    private $format_type;

    //Use dependency injection to lessen the "new" instanciate under construct
    public function __construct(object $player_stats,
        object $players_info,
        object $format_type) {

        $this->player_stats = $player_stats;
        $this->players_info = $players_info;
        $this->format_type = $format_type;
    }

    public function getPlayerStats(object $search) {
        return $this->player_stats->getPlayerDetails($search);
    }

    public function getPlayers(object $search) {
        return $this->players_info->getPlayerDetails($search);
    }

    public function format($data, $format = 'html') {
        // dd($this->format_type);
        // return the right data format
        switch($format) {
            case 'xml':
                $this->format_type->getXml($data);
                break;
            case 'json':
                $this->format_type->getJson($data);
                break;
            case 'csv':
                $this->format_type->getCsv($data);
                break;
            default: // html
                $this->format_type->getDefault($data);
                break;
        }
    }
}

?>
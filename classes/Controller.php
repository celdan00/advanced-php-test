<?php
namespace Classes;

require_once('classes/Exporter.php');
require_once('classes/players/PlayerStats.php');
require_once('classes/players/PlayersInfo.php');
require_once('classes/format/Format.php');

use Classes\Players\PlayerStats;
use Classes\Players\PlayersInfo;
use Classes\Format\Format;
use Classes\Exporter;

class Controller {

    public function __construct($args) {
        $this->args = $args;
    }

    public function export(string $type, string $format) {
        $data = [];
        $player_stats = new PlayerStats;
        $player_info = new PlayersInfo;
        $format_type = new Format;
        $exporter = new Exporter($player_stats,
            $player_info,
            $format_type);
        
        switch ($type) {
            case 'playerstats':
                $search = $this->getSearchedArgs();
                $data = $exporter->getPlayerStats($search);
                break;
            case 'players':
                $search = $this->getSearchedArgs();
                $data = $exporter->getPlayers($search);
                break;
        }
        if (!$data) {
            exit("Error: No data found!");
        }

        return $exporter->format($data, $format);
    }

    protected function getSearchedArgs() {
        $searchArgs = ['player', 'playerId', 'team', 'position', 'country'];
        $search = $this->args->filter(function($value, $key) use ($searchArgs) {
            return in_array($key, $searchArgs);
        });

        return $search;
    }
}
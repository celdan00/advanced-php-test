<?php
namespace Classes\Interface;

interface FormatInterface {
	public function getFormatData(object $data);
}
<?php
namespace Classes\Interface;

interface PlayersInterface {
	public function getPlayerDetails(object $search);
}
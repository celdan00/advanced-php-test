# PHP Senior Technical Test for Zeald

Hi Good Day, My name is Jeff and here is my answer for your given technical exam. These are the files that I added for this web app.

* Players
* PlayersInfo
* PlayerStats
* PlayersInterface
* FormatInterface
* CsvFormat
* JsonFormat
* XmlFormat
* DefaultFormat
* Database

## Database

I moved the declaration of database name into the separate database class so that It'll not be confusing.

```bash
database/classes/Database.php
```
## What are the methods or ways that I use to make the app a little bit better?

I use a combination of OOP and the Single Responsibility Principle and a kind of similar to Open/Closed Principle.


<?php 
namespace Classes;

class Database {

	const db_name = 'nba2019'; //constant value

	protected function connectDb() {
		$mysqli_db = new \mysqli('localhost', 'root', '', self::db_name);
		return $mysqli_db;
	}

    /**
    * Execute a query & return the resulting data as an array of assoc arrays
    * @param string $sql query to execute
    * @return boolean|array array of associative arrays - query results for select
    *     otherwise true or false for insert/update/delete success
    */

    function query($sql) {
        $result = $this->connectDb()->query($sql);
	        if (!is_object($result)) {
	        return $result;
    	}
        $data = [];
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        return $data;

    }
}